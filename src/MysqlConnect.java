import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class MysqlConnect {
    Connection conn;
    
    /*
    Returns a valid Connection Object if it can establish connection with empdb.
    Else returns null
    */
    public static Connection connectDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/questionpapergenerator","rajpanchal","rajpanchal");
            System.out.println("Connection established successfully!!");
            return conn;
        }catch(SQLException e){
            System.out.println("Connection failed!");
            return null;
        }
    }
}
