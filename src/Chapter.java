import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class Chapter {
    private int chpID;
    private int chpNo;
    private String chpName;
    private int weightage;
    private List<Question> ques;
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    
    public Chapter(){
        conn = MysqlConnect.connectDB();
    }

    public Chapter(int chpNo, String chpName, int weightage){
        conn = MysqlConnect.connectDB();
        this.chpNo = chpNo;
        this.chpName = chpName;
        this.weightage = weightage;
        ques = new ArrayList<>();
    }
    //Setters
    public void setChpNo(int chpNo){
        this.chpNo = chpNo;
    }
    public void setChpName(String chpName){
        this.chpName = chpName;
    } 
    public void setWeightage(int weightage){
        this.weightage = weightage;
    }
    //Getters
    public int getChpNo(){
        return this.chpNo;
    }
    public String getChpName(){
        return this.chpName;
    } 
    public int getWeightage(){
        return  this.weightage;
    }
    public int getChpID(){
        try{ 
            ResultSet rs = getChaptersTable();
            while(rs.next())
                if(this.chpName.equals(rs.getString("chp_name"))){
                    this.chpID = rs.getInt(1);
                    return chpID;
                }
        }catch(SQLException sqle){
            System.out.println("Issue while checking subject exists or not : "+sqle);
        }
        return -1;
    }
    public ResultSet getChaptersTable(){
        String sql = "SELECT * FROM chapters";
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
        }catch(SQLException sqle){
            System.out.println("Issue while fetching from chapters : "+sqle);
        }
        return rs;
    }
    public ResultSet getChaptersOf(int subID){
        String sql = "SELECT * FROM chapters WHERE sub_id = ?";
        try{
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subID);
            rs = ps.executeQuery();
        }catch(SQLException e){
            System.out.println("Execption in getCHaptersOf() : "+e);
        }
        return rs;
    }
    
    public boolean insertIntoChapters(int subID){
        try{  
            String sql = "INSERT INTO chapters(sub_id, chp_no, chp_name, chp_weightage) VALUES(?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subID);
            ps.setInt(2, this.chpNo);
            ps.setString(3, this.chpName);
            ps.setInt(4, this.weightage);
            ps.execute();
            System.out.println("Inserted Successfully!");
            return true;
        }catch(SQLException sqle){
            System.out.println("Issue while inserting in subjects : "+sqle);
        }
        return false;
    }
    public boolean deleteFromChapters(int subID){
        try{       
            String sql = "DELETE FROM chapters WHERE sub_id = ? && chp_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subID);
            ps.setInt(2, this.chpNo);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Chapter Deleted Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
            System.out.println("Chapter Deleted Successfully!");
            return true;
        }catch(SQLException sqle){
            System.out.println("Issue while deleting in chapters : "+sqle);
        }
        return false;
    }
    
    public boolean addQuestion(Question question){
        ResultSet rs = getChaptersTable();
        int subID=-1;
        try{
        while(rs.next())
            if(chpName.equals(rs.getString("chp_name")))
                subID = rs.getInt(2);       
        }catch(SQLException sqle){
        }
//        if(this.ques.add(question))//add in question in ArrayList
//            return ques.insertIntoQuestions(subID, this.getChpID(this.chpName));//add question in db
//        else
//            return false;
        return false;
    }
    
//    public boolean deleteQuestion(Chapter chapter){
//        if(this.chps.remove(chapter))
//            return chapter.deleteFromChapters(this.getSubID(this.subName));
//        else
//            return false;
//    }
}
