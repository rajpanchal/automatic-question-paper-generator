
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class Query {
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    public Query(){
        conn = MysqlConnect.connectDB();
    }
//    public boolean insertInto(String tableName, ArrayList<String> attributes, ArrayList<String> values){
//        String sql = "INSERT INTO " +tableName+ "(";
//        for(String attribute : attributes){
//            sql += attribute;
//            if(attributes.size()>1)
//                sql += ",";
//        }
//        sql += ")";
//        if(attributes.size()>1)
//            sql += " VALUES(";
//        else
//            sql += "VALUE(";
//        for(int i=0; i<attributes.size(); i++){
//            sql += "?";
//            if(attributes.size()>1)
//                sql += ",";
//        }
//        sql += ")";
//        System.out.println("Query for inserting : "+sql);
//        try{
//            ps = conn.prepareStatement(sql);
//           
//        }catch(SQLException sqle){
//            System.out.println("Exception while inserting : "+sqle);
//        }
//        return false;
//    }
    public boolean insertIntoSubjects(String subName){
        if(!isSubjectAlreadyExists(subName)){
            String sql = "INSERT INTO subjects(sub_name) VALUE(?)";
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1,subName);
                ps.execute();
                System.out.println("Inserted Successfully!");
                return true;
            }catch(SQLException sqle){
                System.out.println("Issue while inserting in subjects : "+sqle);
            }
        }
        return false;
    }
    public boolean isSubjectAlreadyExists(String subname){
        String sql = "SELECT * FROM subjects";
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next())
                if(subname.equals(rs.getString("sub_name"))){
                    JOptionPane.showMessageDialog(null, "Subject Already Exists!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return true;
                }
        }catch(SQLException sqle){
            System.out.println("Issue while checking subject exists or not : "+sqle);
        }
        return false;
    }
    public boolean insertIntoChapters(String subName, String chpName, int marks){
        String sql1 = "SELECT sub_id from subjects WHERE sub_name = "+subName;
        try{
            ps = conn.prepareStatement(sql1);
            rs = ps.executeQuery();
            while(rs.next()){
                System.out.println(rs.getInt(1));
            }
        }catch(SQLException sqle){
            System.out.println("");
        }
        String sql = "INSERT INTO chapters(sub_id, chp_name) VALUES(?,?)";
        int subId = 0;
        try{
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subId);
            ps.setString(2,subName);
            ps.execute();
            System.out.println("Inserted Successfully!");
            return true;
        }catch(SQLException sqle){
            System.out.println("Issue while inserting in subjects : "+sqle);
        }
        return false;
    }
    
}