
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class Animations {
    Dashboard dash;
    MenuPanel mp;
    Animations(MenuPanel mp){
        this.mp = mp;
    }
    Animations(Dashboard dash){
        this.dash = dash;
    }
    public void animateSlider(JLabel lbl){
        new Thread(new Runnable(){
            @Override
            public void run(){
                while(mp.jpKnob.getY()<lbl.getY()){
                    mp.jpKnob.setBounds(mp.jpKnob.getX(), mp.jpKnob.getY()+2, mp.jpKnob.getWidth(), mp.jpKnob.getHeight());
                    try{
                        Thread.sleep(2);
                    }catch(Exception e){
                        System.out.println("Exception while animating the Slider : "+e);
                    }
                } 
                while(mp.jpKnob.getY()>lbl.getY()){
                    mp.jpKnob.setBounds(mp.jpKnob.getX(), mp.jpKnob.getY()-2, mp.jpKnob.getWidth(), mp.jpKnob.getHeight());
                    try{
                        Thread.sleep(2);
                    }catch(Exception e){
                        System.out.println("Exception while animating the Slider : "+e);
                    }
                }
            }
        }).start();
    }
}
