import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class Subject {
    private int subID;
    private String subName;
    private List<Chapter> chps;
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    
    public Subject(String subName){
        conn = MysqlConnect.connectDB();
        this.subName = subName;
        chps = new ArrayList<>();
    }
    //Setters
    public void setSubID(int subID){
        this.subID = subID;
    }
    public void setSubName(String subName){
        this.subName = subName;
    }
    //Getters
    public int getSubID(){
        try{ 
            ResultSet rs = getSubjectTable();
            while(rs.next()){
                if(this.subName.equals(rs.getString("sub_name"))){
                    this.subID = rs.getInt(1);
                    return subID;
                }
            }
        }catch(SQLException sqle){
            System.out.println("Issue while checking subject exists or not : "+sqle);
        }
        return -1;
    }
    public String getSubName(){
        return this.subName;
    }
    public List<Chapter> getChapters(){
        return chps;
    }
    // Retru ResultSet of 'subject' Table (Whole Table)
    public ResultSet getSubjectTable(){
        String sql = "SELECT * FROM subjects";
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
        }catch(SQLException sqle){
            System.out.println("Issue while fetching from subjects : "+sqle);
        }
        return rs;
    }
    // To check whether subject exists or not
    public boolean isSubjectAlreadyExists(String subname){
        try{ 
            rs = getSubjectTable();
            while(rs.next()){
                if(subname.equals(rs.getString("sub_name"))){
                    JOptionPane.showMessageDialog(null, "Subject Already Exists!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return true;
                }
            }
        }catch(SQLException sqle){
            System.out.println("Issue while checking subject exists or not : "+sqle);
        }
        return false;
    }
    // Inserts the subject
    public boolean insertIntoSubjects(){
        if(!isSubjectAlreadyExists(this.subName)){
            String sql = "INSERT INTO subjects(sub_name) VALUE(?)";
            try{
                ps = conn.prepareStatement(sql);
                ps.setString(1,this.subName);
                ps.execute();
                JOptionPane.showMessageDialog(null, "Subject Entered Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
                System.out.println("Inserted Successfully!");
                return true;
            }catch(SQLException sqle){
                System.out.println("Issue while inserting in subjects : "+sqle);
            }
        }
        return false;
    }
    // Deletes the subject 
    public boolean deleteFromSubjects(){
        if(isSubjectAlreadyExists(this.subName)){
            try{
                this.subID = getSubID();    
                String sql = "DELETE FROM subjects WHERE sub_id = ?";
                ps = conn.prepareStatement(sql);
                ps.setInt(1,this.subID);
                ps.execute();
                JOptionPane.showMessageDialog(null, "Subject Deleted Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
                System.out.println("Deleted Successfully!");
                return true;
            }catch(SQLException sqle){
                System.out.println("Issue while deleting in subjects : "+sqle);
            }
        }
        return false;
    }
    
    public boolean addChapter(Chapter chapter){
        if(this.chps.add(chapter))//add in chapters in ArrayList
            return chapter.insertIntoChapters(this.getSubID());//add chapters in db
        else
            return false;
    }
    
    public boolean deleteChapter(Chapter chapter){
        if(this.chps.remove(chapter))
            return chapter.deleteFromChapters(this.getSubID());
        else
            return false;
    }
}
