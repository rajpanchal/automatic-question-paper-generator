import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class Question {
    private String ques;
    private int mark;
    private int difficulty_lvl;
    private int probability;
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    public Question(String ques, int mark, int difficulty_lvl, int probability){
        conn = MysqlConnect.connectDB();
        this.ques = ques;
        this.mark = mark;
        this.difficulty_lvl = difficulty_lvl;
        this.probability = probability;
    }
    //Setters
    public void setQuestion(String ques){
        this.ques = ques;
    }
    public void setMarks(int mark){
        this.mark = mark;
    }
    public void setDifficultyLvl(int difficulty_lvl){
        this.difficulty_lvl = difficulty_lvl;
    }
    public void setProbability(int probability){
        this.probability = probability;
    }
    //Getters
    public String getQuestion(){
        return this.ques;
    }
    public int getMarks(){
        return this.mark;
    }
    public int getDifficultyLvl(){
        return this.difficulty_lvl;
    }
    public int getProbability(){
        return this.probability;
    }
    
    public boolean insertIntoQuestion(int subID, int chpID){
        try{
            String sql = "INSERT INTO questions(sub_id, chp_id, question, mark, difficulty_lvl, probability) VALUES(?,?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subID);
            ps.setInt(2, chpID);
            ps.setString(3, this.ques);
            ps.setInt(4, this.mark);
            ps.setInt(5, this.difficulty_lvl);
            ps.setInt(6, this.probability);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Question Inserted Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
            System.out.println("Qusetion Inserted Successfully!");
            return true;
        }catch(SQLException sqle){
            System.out.println("Issue while inserting in Question : "+sqle);
        }
        return false;
    }
    public boolean deleteFromQusetions(int subID, int chpID){
        try{       
            String sql = "DELETE FROM questions WHERE sub_id = ? && chp_id = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subID);
            ps.setInt(2, chpID);
            ps.execute();
            JOptionPane.showMessageDialog(null, "Question Deleted Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
            System.out.println("Question Deleted Successfully!");
            return true;
        }catch(SQLException sqle){
            System.out.println("Issue while deleting from 'questions' : "+sqle);
        }
        return false;
    }
}
