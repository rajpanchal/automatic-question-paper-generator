

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */
public class NumericJTextField extends JTextField implements KeyListener{
    private int flag = 0;
    String msg = "";
    public NumericJTextField(){
        addListeners();
    }
    public NumericJTextField(int numChars){
        super(numChars);
        addListeners();
    }
    public void addListeners(){
        addKeyListener(this);
    }
    @Override
    public void keyPressed(KeyEvent ke){
        if (ke.getKeyCode()>=KeyEvent.VK_0 && ke.getKeyCode()<=KeyEvent.VK_9)
            flag = 0;
        else if (ke.getKeyCode() == ke.VK_BACK_SPACE){
            msg = getText().substring(0, getText().length());
            this.setText(msg);
            this.setCaretPosition(msg.length());
            flag = 1;
        }
        else{
            flag = 1;
            ke.consume();
        }
    }
    @Override
    public void keyTyped(KeyEvent ke){
        if (ke.getKeyChar() == ke.CHAR_UNDEFINED)
            System.out.println("Undefined!");
        else if (flag == 0)
            msg = getText();
    }
    @Override
    public void keyReleased(KeyEvent ke){}
}
